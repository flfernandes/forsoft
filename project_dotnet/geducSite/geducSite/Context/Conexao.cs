﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Context
{
    public class Conexao
    {
        //sql server
        protected SqlConnection con;
        protected SqlCommand cmd;
        protected SqlDataReader dr;
        /*
        //mysql
        protected MySqlConnection myCon;
        protected MySqlCommand myCmd;
        protected MySqlDataReader mydr;*/

        protected void AbrirConexao()
        {
            //sql server
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["conexaoSqlServer"].ConnectionString);
            con.Open();
            /*
            //mysql
            myCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["conexaoMySql"].ConnectionString);
            myCon.Open();*/
        }

        protected void FecharConexao()
        {
            //sql server
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            /*
            //mysql
            if (myCon.State == ConnectionState.Open)
            {
                myCon.Close();
            }*/
        }
    }
}