﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class listar_aluno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected IEnumerable<Aluno> listaA()
        {
            return new AlunoDAO().ListarAluno().OrderByDescending(x => x.nome);
        }
    }
}