﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class listar_notas : System.Web.UI.Page
    {

        protected IEnumerable<Nota> todasAsNotas;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["todasAsNotas"] != null)
            {
                todasAsNotas = (IEnumerable<Nota>)Session["todasAsNotas"];
            }
            else
            {
                todasAsNotas = new NotaDAO().Listar();
                Session["todasAsNotas"] = todasAsNotas;
                Session.Timeout = 6000;
            }
        }

        protected IEnumerable<Nota> ListarNotas()
        {
            return todasAsNotas.OrderByDescending(x => x.disciplina.nome);
        }

    }
}