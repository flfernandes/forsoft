﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="alterar_nota.aspx.cs" Inherits="geducSite.View.alterar_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <form id="frmNota" runat="server">
        <br />
        <h2>Notas</h2>
        <br />
        <div>
            <asp:Label ID="Label1" runat="server" Text="identificação:" Width="70" />
            <asp:TextBox ID="txtid" runat="server" />
            <asp:Button ID="btnBuscar" Text="Preencher" runat="server" OnClick="btnBuscar_Click" />
            <br />
            <br />
            <asp:Label ID="lblDate" runat="server" Text="Data:" Width="70" />
            <asp:TextBox ID="txtDate" runat="server" />
            <br />
            <br />
            <asp:Label ID="lblNota" runat="server" Text="Nota :" Width="70" />
            <asp:TextBox ID="txtNota" runat="server" />
            <br />
            <br />
            <asp:Label ID="lblPeriodo" runat="server" Text="Periodo:" Width="70" />
            <asp:TextBox ID="txtPeriodo" runat="server" />
            <br />
            <br />

            <asp:Label ID="lblOrigem" runat="server" Text="Origem:" Width="70" />
            <asp:TextBox ID="txtOrigem" runat="server" />
            <br />
            <br />
        </div>
        <br />
        <asp:Label ID="lblMensagem" runat="server" />
        <br />
        <asp:Button ID="btnAlterarNota" class="#" runat="server" Text="Alte Nota" OnClick="btnAlterarNota_Click" />
    </form>

</asp:Content>
