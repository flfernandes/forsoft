﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="listar_aluno.aspx.cs" Inherits="geducSite.View.listar_aluno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
            <tr >
                <th>Nome:</th>
                <th>Sexo;</th>
                <th>Situacao;</th>    
                <th>Telefone:</th>
                <th>CPF:</th>    
                <th>RG:</th> 
                <th></th>   
            </tr>
            <%foreach(var lista in listaA()){ %>
            <tr>
                <td><%: lista.nome %></td>
                <td><%: lista.sexo %></td>
                <td><%: lista.situacao %></td>
                <td><%: lista.contato.telefoneFixo %></td>
                <td><%: lista.documento.cpf %></td>
                <td><%: lista.documento.rg %></td>
            </tr>
            <%} %>
    </table>
</asp:Content>
