﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> todosOsFuncionario;
        protected IEnumerable<ProgramaSocial> TodosOsProgramasSociais;
        protected IEnumerable<Cargo> TodosOsCargos;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todosOsFuncionario = (IEnumerable<Funcionario>)Session["ListaFuncionarios"];
            }
            else
            {
                todosOsFuncionario = new FuncionarioDAO().Listar();
                Session["ListaFuncionarios"] = todosOsFuncionario;
                Session.Timeout = 6000;
            }


            //guardar todos os cargos para amis tarde
            if (Session["TodosOsCargos"] != null)
            {
                TodosOsCargos = (IEnumerable<Cargo>)Session["TodosOsCargos"];
            }
            else
            {
                TodosOsCargos = new CargoDAO().Listar();
                Session["TodosOsCargos"] = TodosOsCargos;
                Session.Timeout = 6000;
            }


            //guardar todos os programas para amis tarde
            if (Session["TodosOsProgramasSociais"] != null)
            {
                TodosOsProgramasSociais = (IEnumerable<ProgramaSocial>)Session["TodosOsProgramasSociais"];
            }
            else
            {
                TodosOsProgramasSociais = new ProgramaSocialDAO().listar();
                Session["TodosOsProgramasSociais"] = TodosOsProgramasSociais;
                Session.Timeout = 6000;
            }


            ddlCargos.DataSource = TodosOsCargos;
            ddlCargos.DataValueField = "idCargo";
            ddlCargos.DataTextField = "cargo";
            ddlCargos.DataBind();

            ddlProgramaSocial.DataSource = TodosOsProgramasSociais;
            ddlProgramaSocial.DataValueField = "idProgramaSocial";
            ddlProgramaSocial.DataTextField = "nomePrograma";
            ddlProgramaSocial.DataBind();
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Funcionario funcionario = new Funcionario();
            funcionario = BuscarFuncionario();
            if (funcionario != null)
            {
                txtUsuario.Text = funcionario.login.usuario;
                txtSenha.Text = funcionario.login.senha;
                ddlPerfilAcesso.SelectedValue = funcionario.login.perfilAcesso;
                txtNomeFuncionario.Text = funcionario.nome;
                txtMatricula.Text = funcionario.matricula;
                txtDataNascimento.Text = Convert.ToString(funcionario.dataNascimento);
                txtDataCertidaoNascimento.Text = Convert.ToString(funcionario.documento.dataEmiCertidao);
                if (funcionario.sexo == "Masculino")
                {
                    rbSexoM.Checked = true;
                }
                else if (funcionario.sexo == "Feminino")
                {
                    rbSexoF.Checked = true;
                }
                txtNaturalidade.Text = funcionario.naturalidade;
                txtNacionalidade.Text = funcionario.nacionalidade;
                txtNomePai.Text = funcionario.nomePai;
                txtNomeMae.Text = funcionario.nomeMae;
                ddlEtnia.SelectedValue = funcionario.etnia;
                ddlEstadoCivil.SelectedValue = funcionario.estadoCivil;
                ddlEscolaridade.SelectedValue = funcionario.nivelEscolaridade;
                ddlCargos.SelectedValue = Convert.ToString(funcionario.cargo.idCargo);
                ddlSituacao.SelectedValue = funcionario.situacao;
                if (rbNecessidadeSim.Checked)
                {
                    if (funcionario.necessidadeEsp == "Sim")
                        rbNecessidadeSim.Checked = true;
                }
                else if (funcionario.necessidadeEsp == "Não")
                {
                    rbNecessidadeNao.Checked = true;
                }
                ddlEtnia.SelectedValue = funcionario.situacao;
                txtCpf.Text = funcionario.documento.cpf;
                txtrg.Text = funcionario.documento.rg;
                txtExpedido.Text = Convert.ToString(funcionario.documento.dataExpedicao);
                txtOrgao.Text = funcionario.documento.orgaoExpedidor;
                txtNumCertificadoNascimento.Text = funcionario.documento.certReservista;
                txtLivroCertidaoNascimento.Text = funcionario.documento.livroCertidao;
                txtFolhaCertidaoNascimento.Text = funcionario.documento.folhaCertidao;
                txtDataCertidaoNascimento.Text = Convert.ToString(funcionario.documento.dataEmiCertidao);
                txtTituloEleito.Text = funcionario.documento.titEleitor;
                txtCertificadoReservista.Text = funcionario.documento.certReservista;
                txtLogradouro.Text = funcionario.endereco.longradouro;
                txtNumero.Text = funcionario.endereco.numero;
                txtComplemento.Text = funcionario.endereco.complemento;
                txtBairro.Text = funcionario.endereco.bairro;
                txtCidade.Text = funcionario.endereco.cidade;
                txtCEP.Text = funcionario.endereco.cep;
                ddlUF.Text = funcionario.endereco.uf;
                txtMunicipio.Text = funcionario.endereco.municipio;
                txtZona.Text = funcionario.endereco.zona;
                txtTelefone.Text = funcionario.contato.telefoneFixo;
                txtCelular.Text = funcionario.contato.telefoneCelular;
                txtEmail.Text = funcionario.contato.email;
                txtOutros.Text = funcionario.contato.outros;
                ddlProgramaSocial.SelectedValue = Convert.ToString(funcionario.programaSocial.idProgramaSocial);
            }

        }

        protected Funcionario BuscarFuncionario()
        {
            Funcionario f = null;
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            if (txtBuscar.Text.Length != 0)
            {
                switch (valor)
                {
                    case 1:
                        f = todosOsFuncionario.SingleOrDefault(x => x.documento.cpf.ToUpper() == txtBuscar.Text.ToUpper() || x.documento.cpf.ToUpper() == txtCpf.Text.ToUpper());
                        break;
                    case 2:
                        f = todosOsFuncionario.SingleOrDefault(x => x.matricula.ToUpper() == txtBuscar.Text.ToUpper() || x.documento.cpf.ToUpper() == txtMatricula.Text.ToUpper());
                        break;
                }
            }
            return f;
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            String[] campos = new String[] { txtUsuario.Text, txtSenha.Text, txtNomeFuncionario.Text, txtDataNascimento.Text, /*sexo,*/
                txtNacionalidade.Text, ddlEscolaridade.SelectedValue, ddlEtnia.SelectedValue, txtEmail.Text, ddlEstadoCivil.SelectedValue,
                txtLogradouro.Text, txtNumero.Text, txtBairro.Text, txtCidade.Text, ddlUF.SelectedValue, txtCEP.Text, txtMunicipio.Text };
            String[] camposSomenteNumero = new String[] { txtCpf.Text, txtCelular.Text, txtTelefone.Text, txtCEP.Text };
            String[] camposSomenteLetras = new String[] { txtNomeFuncionario.Text, txtNaturalidade.Text, txtNacionalidade.Text, txtNomePai.Text,
                txtNomeMae.Text, txtCidade.Text, txtMunicipio.Text };
            String[] camposData = new String[] { txtDataNascimento.Text, txtExpedido.Text, txtDataCertidaoNascimento.Text };
            if (!Validador.seTudoVazio(campos))
            {
                if (Validador.seSomenteLetra(camposSomenteLetras) && Validador.seSomenteNumero(camposSomenteNumero) && Validador.seData(camposData)
                    && Validar.seCEP(txtCEP.Text) && Validar.seCPF(txtCpf.Text) && Validar.seEmail(txtEmail.Text))
                {
                    Funcionario funcionario = new Funcionario();

                    funcionario = BuscarFuncionario();
                    if (funcionario != null)
                    {
                        funcionario.login.usuario = txtUsuario.Text;
                        funcionario.login.senha = txtSenha.Text;
                        funcionario.login.perfilAcesso = ddlPerfilAcesso.SelectedValue;
                        funcionario.nome = txtNomeFuncionario.Text;
                        funcionario.matricula = txtMatricula.Text;
                        funcionario.dataNascimento = Convert.ToDateTime(txtDataNascimento.Text);
                        if (rbSexoM.Checked == true)
                        {
                            funcionario.sexo = "Masculino";
                        }
                        else if (rbSexoF.Checked == true)
                        {
                            funcionario.sexo = "Feminino";
                        }
                        funcionario.naturalidade = txtNaturalidade.Text;
                        funcionario.nacionalidade = txtNacionalidade.Text;
                        funcionario.nomePai = txtNomePai.Text;
                        funcionario.nomeMae = txtNomeMae.Text;
                        funcionario.etnia = ddlEtnia.SelectedValue;
                        funcionario.estadoCivil = ddlEstadoCivil.SelectedValue;
                        funcionario.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                        funcionario.situacao = ddlSituacao.SelectedValue;
                        funcionario.cargo = TodosOsCargos.SingleOrDefault(x => x.idCargo == Convert.ToInt32(ddlCargos.SelectedValue));
                        funcionario.documento.cpf = txtCpf.Text;
                        funcionario.documento.rg = txtrg.Text;
                        funcionario.documento.dataExpedicao = Convert.ToDateTime(txtExpedido.Text);
                        funcionario.documento.orgaoExpedidor = txtOrgao.Text;
                        funcionario.documento.numCertidao = txtNumCertificadoNascimento.Text;
                        funcionario.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                        funcionario.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                        funcionario.documento.dataEmiCertidao = Convert.ToDateTime(txtDataCertidaoNascimento.Text);
                        funcionario.documento.titEleitor = txtTituloEleito.Text;
                        funcionario.documento.certReservista = txtCertificadoReservista.Text;
                        funcionario.endereco.longradouro = txtLogradouro.Text;
                        funcionario.endereco.numero = txtNumero.Text;
                        funcionario.endereco.complemento = txtComplemento.Text;
                        funcionario.endereco.bairro = txtBairro.Text;
                        funcionario.endereco.cidade = txtCidade.Text;
                        funcionario.endereco.cep = txtCEP.Text;
                        funcionario.endereco.uf = ddlUF.SelectedValue;
                        funcionario.endereco.municipio = txtMunicipio.Text;
                        funcionario.endereco.zona = txtZona.Text;
                        funcionario.contato.telefoneFixo = txtTelefone.Text;
                        funcionario.contato.telefoneCelular = txtCelular.Text;
                        funcionario.contato.email = txtEmail.Text;
                        funcionario.contato.outros = txtOutros.Text;
                        funcionario.programaSocial = TodosOsProgramasSociais.SingleOrDefault(x => x.idProgramaSocial == Convert.ToInt32(ddlProgramaSocial.SelectedValue));

                        new FuncionarioDAO().Update(funcionario);

                        Response.Redirect("alterar_funcionario.aspx");
                    }
                }
            }
        }
    }
}