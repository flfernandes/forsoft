﻿using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using geducSite.Models;
using System.Collections.ObjectModel;

namespace geducSite.View
{
    public partial class cadastro_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> TodosOsFuncionarios;
        protected IEnumerable<ProgramaSocial> TodosOsProgramasSociais;
        protected IEnumerable<Cargo> TodosOsCargos;

        protected void Page_Load(object sender, EventArgs e)
        {
            TodosOsFuncionarios = new FuncionarioDAO().Listar();
            TodosOsProgramasSociais = new ProgramaSocialDAO().listar();
            TodosOsCargos = new CargoDAO().Listar();

            ddlCargos.DataSource = TodosOsCargos;
            ddlCargos.DataValueField = "idCargo";
            ddlCargos.DataTextField = "cargo";
            ddlCargos.DataBind();

            ddlProgramaSocial.DataSource = TodosOsProgramasSociais;
            ddlProgramaSocial.DataValueField = "idProgramaSocial";
            ddlProgramaSocial.DataTextField = "nomePrograma";
            ddlProgramaSocial.DataBind();
        }

        protected void AddNaSession(Funcionario f)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                var addSession = (Collection<Funcionario>)Session["ListaFuncionarios"];
                addSession.Add(f);
                Session["ListaFuncionarios"] = (IEnumerable<Funcionario>)addSession;
            }
        }

        protected void Cadastrar_Click(object sender, EventArgs e)
        {
            String[] campos = new String[] { txtUsuario.Text, txtSenha.Text, txtNomeFuncionario.Text, txtDataNascimento.Text, /*sexo,*/
                txtNacionalidade.Text, ddlEscolaridade.SelectedValue, ddlEtnia.SelectedValue, txtEmail.Text, ddlEstadoCivil.SelectedValue,
                txtLogradouro.Text, txtNumero.Text, txtBairro.Text, txtCidade.Text, ddlUF.SelectedValue, txtCEP.Text, txtMunicipio.Text };
            String[] camposSomenteNumero = new String[] { txtCpf.Text, txtCelular.Text, txtTelefone.Text, txtCEP.Text };
            String[] camposSomenteLetras = new String[] { txtNomeFuncionario.Text, txtNaturalidade.Text, txtNacionalidade.Text, txtNomePai.Text,
                txtNomeMae.Text, txtCidade.Text, txtMunicipio.Text };
            String[] camposData = new String[] { txtDataNascimento.Text, txtExpedido.Text, txtDataCertidaoNascimento.Text };
            if (!Validador.seAlgumVazio(campos))
            {
                if (Validador.seSomenteLetra(camposSomenteLetras) && Validador.seSomenteNumero(camposSomenteNumero) && Validador.seData(camposData)
                    && Validar.seCEP(txtCEP.Text) && Validar.seCPF(txtCpf.Text) && Validar.seEmail(txtEmail.Text))
                {
                    try
                    {
                        Funcionario f = new Funcionario();
                        f.nome = txtNomeFuncionario.Text;
                        f.matricula = txtMatricula.Text;
                        f.dataNascimento = DateTime.Parse(txtDataNascimento.Text);
                        if (rbSexoM.Checked == true)
                        {
                            f.sexo = "Masculino";
                        }
                        else if (rbSexoF.Checked == true)
                        {
                            f.sexo = "Feminino";
                        }
                        f.naturalidade = txtNaturalidade.Text;
                        f.nacionalidade = txtNacionalidade.Text;
                        f.nomePai = txtNomePai.Text;
                        f.nomeMae = txtNomeMae.Text;
                        f.etnia = ddlEtnia.SelectedValue;
                        f.estadoCivil = ddlEstadoCivil.SelectedValue;
                        f.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                        if (rbNecessidadeSim.Checked)
                        {
                            f.necessidadeEsp = "Sim";
                        }
                        else if (rbNecessidadeNao.Checked)
                        {
                            f.necessidadeEsp = "Não";
                        }
                        f.cargo = TodosOsCargos.SingleOrDefault(x => x.idCargo == Convert.ToInt32(ddlCargos.SelectedValue));
                        f.situacao = ddlSituacao.SelectedValue;

                        geducSite.Models.Login lg = new geducSite.Models.Login();
                        lg.usuario = txtUsuario.Text;
                        lg.senha = txtSenha.Text;
                        lg.perfilAcesso = ddlPerfilAcesso.SelectedValue;

                        f.login = lg;

                        Documento d = new Documento();
                        d.cpf = txtCpf.Text;


                        d.rg = txtrg.Text;
                        d.dataExpedicao = DateTime.Parse(txtExpedido.Text);
                        d.orgaoExpedidor = txtExpedido.Text;
                        d.numCertidao = txtNumCertificadoNascimento.Text;
                        d.livroCertidao = txtLivroCertidaoNascimento.Text;
                        d.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                        d.dataEmiCertidao = DateTime.Parse(txtDataCertidaoNascimento.Text);
                        d.titEleitor = txtTituloEleito.Text;
                        d.certReservista = txtCertificadoReservista.Text;

                        f.documento = d;

                        Endereco en = new Endereco();


                        en.longradouro = txtLogradouro.Text;
                        en.numero = txtNumero.Text;
                        en.complemento = txtComplemento.Text;
                        en.bairro = txtBairro.Text;
                        en.cidade = txtCidade.Text;
                        en.cep = txtCEP.Text;
                        en.uf = ddlUF.SelectedValue;
                        en.municipio = txtMunicipio.Text;
                        en.zona = txtZona.Text;

                        f.endereco = en;

                        Contato con = new Contato();

                        con.telefoneFixo = txtTelefone.Text;
                        con.telefoneCelular = txtCelular.Text;
                        con.email = txtEmail.Text;
                        con.outros = txtOutros.Text;

                        f.contato = con;

                        ProgramaSocial ps = new ProgramaSocial();

                        if (rbProgramaSocialS.Checked == true)
                        {
                            f.programaSocial = TodosOsProgramasSociais.SingleOrDefault(x => x.idProgramaSocial == Convert.ToInt32(ddlProgramaSocial.SelectedValue));
                        }
                        else
                        {
                            f.programaSocial = TodosOsProgramasSociais.SingleOrDefault(x => x.idProgramaSocial == 1); //sugiro colocar o primeiro item da tabela programaSocial como nomePrograma="Não Existemte" ou "Não tem"
                        }


                        new FuncionarioDAO().Salvar(f);
                        AddNaSession(f);

                        Limpar();

                        Response.Redirect("cadastro_funcionario.aspx");
                    }
                    catch (Exception erro)
                    {
                        erro.ToString();
                    }
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Response.Redirect("consultar_funcionario.aspx");
        }

        protected void brnListar_Click(object sender, EventArgs e)
        {
            Response.Redirect("listar_funcionario.aspx");
        }

        protected void Limpar()
        {

            txtUsuario.Text = string.Empty;
            txtSenha.Text = string.Empty;
            txtNomeFuncionario.Text = string.Empty;
            txtDataNascimento.Text = string.Empty;
            txtDataCertidaoNascimento.Text = string.Empty;
            rbSexoF.Checked = false;
            rbSexoM.Checked = false;
            txtNaturalidade.Text = string.Empty;
            txtNacionalidade.Text = string.Empty;
            txtNomePai.Text = string.Empty;
            txtNomeMae.Text = string.Empty;
            rbNecessidadeNao.Text = string.Empty;
            txtrg.Text = string.Empty;
            txtExpedido.Text = string.Empty;
            txtOrgao.Text = string.Empty;
            txtNumCertificadoNascimento.Text = string.Empty;
            txtLivroCertidaoNascimento.Text = string.Empty;
            txtFolhaCertidaoNascimento.Text = string.Empty;
            txtDataCertidaoNascimento.Text = string.Empty;
            txtTituloEleito.Text = string.Empty;
            txtCertificadoReservista.Text = string.Empty;
            txtLogradouro.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtComplemento.Text = string.Empty;
            txtBairro.Text = string.Empty;
            txtCEP.Text = string.Empty;
            txtMunicipio.Text = string.Empty;
            txtZona.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtCelular.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtOutros.Text = string.Empty;
            rbProgramaSocialN.Text = string.Empty;
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }
        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            /*
            try
            {
                Funcionario f = TodosOsFuncionarios.SingleOrDefault(x => x.idFuncionario == Convert.ToInt32(ddlIdFuncionario.SelectedValue));
                f.login.usuario = txtUsuario.Text;
                f.login.senha = txtSenha.Text;
                f.login.perfilAcesso = ddlPerfilAcesso.SelectedValue;
                f.nome = txtNomeFuncionario.Text;
                f.dataNascimento = DateTime.Parse(txtDataNascimento.Text);
                if (rbSexoF.Checked == true)
                {
                    f.sexo = rbSexoF.Text;
                }
                else if (rbSexom.Checked == true)
                {
                    f.sexo = rbSexom.Text;

                }
                f.naturalidade = txtNaturalidade.Text;
                f.nacionalidade = txtNacionalidade.Text;
                f.nomePai = txtNomePai.Text;
                f.nomeMae = txtNomeMae.Text;
                f.etnia = ddlEtnia.SelectedValue;
                f.estadoCivil = ddlEstadoCivil.SelectedValue;
                f.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                f.necessidadeEsp = rbNecessidadeSim.Text;
                f.cargo = TodosOsCargos.SingleOrDefault(x=>x.idCargo == Convert.ToInt32(ddlCargos.SelectedValue));
                f.situacao = ddlSituacao.SelectedValue;
                f.documento.cpf = txtCpf.Text;
                f.documento.rg = txtrg.Text;
                f.documento.dataExpedicao = DateTime.Parse(txtExpedido.Text);
                f.documento.orgaoExpedidor = txtExpedido.Text;
                f.documento.numCertidao = txtNumCertificadoNascimento.Text;
                f.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                f.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                f.documento.dataEmiCertidao = DateTime.Parse(txtDataCertidaoNascimento.Text);
                f.documento.titEleitor = txtTituloEleito.Text;
                f.documento.certReservista = txtCertificadoReservista.Text;
                f.endereco.longradouro = txtLogradouro.Text;
                f.endereco.numero = txtNumero.Text;
                f.endereco.complemento = txtComplemento.Text;
                f.endereco.bairro = txtBairro.Text;
                f.endereco.cidade = txtCidade.Text;
                f.endereco.cep = txtCEP.Text;
                f.endereco.uf = ddlUF.SelectedValue;
                f.endereco.municipio = txtMunicipio.Text;
                f.endereco.zona = txtZona.Text;
                f.contato.telefoneFixo = txtTelefone.Text;
                f.contato.telefoneCelular = txtCelular.Text;
                f.contato.email = txtEmail.Text;
                f.contato.outros = txtOutros.Text;

                ProgramaSocial ps = new ProgramaSocial();

                if (rbProgramaSocialS.Checked == true)
                {
                    ps.idProgramaSocial = Convert.ToInt32(ddlProgramaSocial.SelectedValue);
                }
                else
                {
                    ps.idProgramaSocial = 0; //sugiro colocar o primeiro item da tabela programaSocial como nomePrograma="Não Existemte" ou "Não tem"
                }

                f.programaSocial = ps;

                new FuncionarioDAO().Update(f);

                Limpar();
                Response.Redirect("cadastro_funcionario.aspx");

            }
            catch (Exception erro)
            {
                erro.ToString();
            }
            */
        }

    }
}