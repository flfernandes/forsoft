﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consultar_disciplina.aspx.cs" Inherits="geducSite.View.consultar_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <form id="Formulario" runat="server">
        <h1>Funcion&aacute;rio </h1>
        <br />
        <asp:Label Text="Buscar" runat="server" />
        <br />
        <div>
            <asp:TextBox ID="txtBuscar" runat="server" />
            <asp:DropDownList ID="ddlTipoDeBusca" CssClass="color0" runat="server">
                <asp:ListItem Value="1">Nome</asp:ListItem>
                <asp:ListItem Value="2">Codigo</asp:ListItem>
                <asp:ListItem Value="3">Carga Horaria</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-primary" Text="Buscar" OnClick="btnBuscar_Click" />
        </div>

        <br />
    </form>




    <div id="DivBusca" runat="server" visible="false">

        <% foreach (var lista in BuscarDisciplina())
           { %>
        <div class="alert-success padding-15">
            <asp:Label runat="server" Text="Nome:"></asp:Label>
            <label><%: lista.nome %></label>
            <br />
            <br />
            <asp:Label runat="server" Text="Codigo:"></asp:Label>
            <label><%: lista.codigo %></label>

            <br />
            <br />
            <asp:Label runat="server" Text="Carga Horaria:"></asp:Label>
            <label><%: lista.cargaHoraria %></label>

        </div>
        <% } %>
    </div>
</asp:Content>
