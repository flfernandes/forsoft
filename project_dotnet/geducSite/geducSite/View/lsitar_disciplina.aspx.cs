﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class lsitar_disciplina : System.Web.UI.Page
    {

        protected IEnumerable<Disciplina> todasAsDisciplina;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todasAsDisciplina = (IEnumerable<Disciplina>)Session["todasAsDisciplina"];
            }
            else
            {
                todasAsDisciplina = new DisciplinaDAO().ListarDisciplinas();
                Session["todasAsDisciplina"] = todasAsDisciplina;
                Session.Timeout = 6000;
            }
        }

        protected IEnumerable<Disciplina> ListarDisciplina()
        {
            return todasAsDisciplina.OrderByDescending(x => x.nome);
        }
    }
}