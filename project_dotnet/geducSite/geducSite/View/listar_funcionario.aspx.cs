﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class listar_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> todosOsFuncionario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todosOsFuncionario = (IEnumerable<Funcionario>)Session["ListaFuncionarios"];
            }
            else
            {
                todosOsFuncionario = new FuncionarioDAO().Listar();
                Session["ListaFuncionarios"] = todosOsFuncionario;
                Session.Timeout = 6000;
            }
        }

        protected IEnumerable<Funcionario> ListarFuncionaio()
        {
            return todosOsFuncionario.OrderByDescending(x => x.nome);
        }
        
    }
}