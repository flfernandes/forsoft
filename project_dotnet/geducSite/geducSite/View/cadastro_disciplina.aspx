﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_disciplina.aspx.cs" Inherits="geducSite.View.cadastro_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
        <div class="box-content">
            <form id="form1" runat="server">
                <fieldset>
                    <legend class="text-center">Disciplina</legend>
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCodigoDisciplina" runat="server" Text="Codigo Disciplina:"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtCodigoDisciplina" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeDisciplina" runat="server" Text="Nome Disciplina:"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNomeDisciplina" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCargaHorariaDisciplina" runat="server" Text="Carga Horaria Disciplina:"></asp:Label>

                        <div class="col-sm-10">
                            <asp:TextBox ID="txtCargaHorariaDisciplina" runat="server" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                </fieldset>
                <p>
                    <span id="msg" runat="server"></span>
                </p>
                <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
            </form>
        </div>
    </div>
</asp:Content>
