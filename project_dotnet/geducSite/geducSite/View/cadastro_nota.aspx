﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_nota.aspx.cs" Inherits="geducSite.View.cadastro_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">

    <form id="frmNota" runat="server">
                <br />
                <h2>Notas</h2>
            <br />
            <div>
                <asp:Label ID="lblDate" runat="server" Text="Data:" Width="70" />
                <asp:TextBox ID="txtDate" runat="server" required="required" type="date" />
                <br />
                <br />
                <asp:Label ID="lblAluno" runat="server" Text="Aluno Matricula:" Width="70" />
                <asp:DropDownList ID="ddlAluno" runat="server" /><br />
                <br />
                <br />
                <asp:Label ID="lblDisciplina" runat="server" Text="Disciplina:" Width="70" />
                <asp:DropDownList ID="ddlDisciplina" runat="server" />
                <br />
                <br />
                <asp:Label ID="lblNota" runat="server" Text="Nota :" Width="70" />
                <asp:TextBox ID="txtNota" runat="server" />
                <br />
                <br />
                <asp:Label ID="lblPeriodo" runat="server" Text="Periodo:" Width="70" />
                <asp:TextBox ID="txtPeriodo" runat="server" />
                <br />
                <br />

                <asp:Label ID="lblOrigem" runat="server" Text="Origem:" Width="70" />
                <asp:TextBox ID="txtOrigem" runat="server" />
                <br />
                <br />
            </div>
            <br />
            <asp:Label ID="lblMensagem" runat="server" />
            <br />
            <asp:Button ID="btnAdicionarNota" class="#" runat="server" Text="Adicionar Nota" OnClick="btnAdicionarNota_Click" />
    </form>

</asp:Content>
