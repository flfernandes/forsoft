﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    /// <summary>
    /// estou passando por herança para funcionario todas os atributos de pesso inclusive os que estão sendo relacionados com pessoa 
    /// através das classes endereco, contato.
    /// </summary>
    public class Funcionario : Pessoa // herença - permite que funcionario utilize todos os atributos da classe pessoa
    {
        public int idFuncionario { get; set; }
        [Required]
        public string matricula { get; set; }
        [Required]
        public string situacao { get; set; }
        [Required]
        public Cargo cargo { get; set; }
    }
}
