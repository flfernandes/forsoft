﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Validador
    {
        public static Boolean seAlgumVazio(String[] valores)
        {
            bool result = false;
            foreach (String s in valores)
                result = result || Validar.seVazio(s);
            return result;
        }

        public static Boolean seTudoVazio(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seVazio(s);
            return result;
        }

        public static Boolean seSomenteNumero(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seSomenteNumero(s);
            return result;
        }

        public static Boolean seSomenteLetra(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seSomenteLetra(s);
            return result;
        }

        public static Boolean seCPF(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seCPF(s);
            return result;
        }

        public static Boolean seData(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seData(s);
            return result;
        }

        public static Boolean seCEP(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seCEP(s);
            return result;
        }

        public static Boolean seEmail(String[] valores)
        {
            bool result = true;
            foreach (String s in valores)
                result = result && Validar.seEmail(s);
            return result;
        }
    }
}