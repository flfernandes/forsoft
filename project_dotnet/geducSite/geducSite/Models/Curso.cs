﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;


namespace geducSite.Models
{
    public class Curso
    {
        public int idCurso { get; set; }
        public string nomeC { get; set; }
        public string descricao { get; set; }
        public int cargaHoraria { get; set; }
        public string codigoC { get; set; }
        public string modalidadeDeEnsino { get; set; }
        public string nivelEnsino { get; set; }

        public Aluno aluno { get; set; }

    }
}