﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.View;

namespace geducSite.Models
{
    public class Validar
    {
        /*Validando e-mail*/
        public static Boolean seEmail(String Valor)
        {
            bool email = false;
            int verificarArroba = Valor.IndexOf("@");
            if (verificarArroba > 0)
            {
                int verificarPonto = Valor.IndexOf(".", verificarArroba);
                if (verificarPonto - 1 > verificarArroba)
                {
                    if (verificarPonto + 1 < Valor.Length)
                    {
                        String indexDot2 = Valor.Substring(verificarPonto + 1, 1);
                        if (indexDot2 != ".")
                        {
                            email = true;
                        }
                    }
                }
            }
            return email;
        }

        /*Validando campos obrigatórios*/

        public static Boolean seVazio(String valor) { return String.IsNullOrEmpty(valor.ToString()); }


        /*Validando campo com somente números*/

        public static Boolean seSomenteNumero(String valor)
        {
            foreach (char c in valor)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        /*Validando campo com somente letras*/

        public static Boolean seSomenteLetra(String valor)
        {
            foreach (char c in valor)
            {
                if (c != ' ' && (c < 'A' || (c > 'Z' && c < 'a') || c > 'z'))
                    return false;
            }
            return true;
        }

        /*Validando o CEP*/

        public static Boolean seCEP(String cep)
        {
            if (cep.Length == 8)
            {
                cep = cep.Substring(0, 5) + "-" + cep.Substring(5, 3);
                //txt.Text = cep;
            }
            return System.Text.RegularExpressions.Regex.IsMatch(cep, ("[0-9]{5}-[0-9]{3}"));
        }

        public static Boolean seData(String valor)
        {
            DateTime result;
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR");
            return System.DateTime.TryParse(valor, culture, System.Globalization.DateTimeStyles.None, out result);
        }

        public static Boolean seCPF(String valor)
        {
            if (valor.Length - valor.Replace(".", "").Length > 3 || valor.Length - valor.Replace("-", "").Length > 1) return false;
            valor = valor.Replace(".", "");
            valor = valor.Replace("-", "");
            if (valor.Length != 11) return false;

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
            {
                if (valor[0] != valor[i]) igual = false;
            }
            if (igual || valor == "12345678909") return false;

            int[] digitos = new int[11];
            for (int i = 0; i < 11; i++)
            {
                digitos[i] = valor[i] - 48;
            }

            int resultado = 0;
            for (int i = 0; i < 9; i++)
                resultado += (10 - i) * digitos[i];
            resultado %= 11;
            if (resultado == 0 || resultado == 1)
            {
                if (digitos[9] != 0) return false;
            }
            else if (digitos[9] != 11 - resultado) return false;

            resultado = 0;
            for (int i = 0; i < 10; i++)
                resultado += (11 - i) * digitos[i];
            resultado %= 11;
            if (resultado == 0 || resultado == 1)
            {
                if (digitos[10] != 0) return false;
            }
            else if (digitos[10] != 11 - resultado) return false;

            return true;
        }
    }
}