﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Context;
using geducSite.Models;
using System.Data.SqlClient;
//using MySql.Data.MySqlClient;

namespace geducSite.Persistence
{
    public class AlunoDAO : Conexao
    {
        
        public void salvar(Aluno a)
        {

            try
            {
                AbrirConexao();

                //SQL SERVER

                //Gravar o Login Sql Server
                cmd = new SqlCommand("INSERT INTO login (usuario, senha, perfilAcesso) VALUES (@v1, @v2, @v3) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.login.usuario);
                cmd.Parameters.AddWithValue("@v2", a.login.senha);
                cmd.Parameters.AddWithValue("@v3", a.login.perfilAcesso);
                a.login.idLogin = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Documento no Sql Server
                cmd = new SqlCommand("INSERT INTO documento (cpf, rg, dataExpedicao, orgaoExpedidor, numCertidao, livroCertidao, folhaCertidao, dataEmiCertidao, titEleitor, certReservista) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.documento.cpf);
                cmd.Parameters.AddWithValue("@v2", a.documento.rg);
                cmd.Parameters.AddWithValue("@v3", a.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", a.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", a.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", a.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", a.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", a.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", a.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", a.documento.certReservista);
                a.documento.idDocumento = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Endereco no Sql server
                cmd = new SqlCommand("INSERT INTO endereco (logradouro, numero, complemento, bairro, cidade, uf, cep, municipio, zona) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", a.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", a.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", a.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", a.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", a.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", a.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", a.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", a.endereco.zona);
                a.endereco.idEndereco = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Contato no Sql Server
                cmd = new SqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular, email, outros) VALUES (@v1, @v2, @v3, @v4) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", a.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", a.contato.email);
                cmd.Parameters.AddWithValue("@v4", a.contato.outros);
                a.contato.idContato = Convert.ToInt32(cmd.ExecuteScalar());

                //Pessoa no Sql Server
                cmd = new SqlCommand("INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento, idProgramaSocial) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.nome);
                cmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", a.sexo);
                cmd.Parameters.AddWithValue("@v4", a.naturalidade);
                cmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", a.nomePai);
                cmd.Parameters.AddWithValue("@v7", a.nomeMae);
                cmd.Parameters.AddWithValue("@v8", a.etnia);
                cmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", a.login.idLogin);
                cmd.Parameters.AddWithValue("@v13", a.contato.idContato);
                cmd.Parameters.AddWithValue("@v14", a.endereco.idEndereco);
                cmd.Parameters.AddWithValue("@v15", a.documento.idDocumento);
                cmd.Parameters.AddWithValue("@v16", a.programaSocial.idProgramaSocial);
                a.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());

                //Aluno Sql Server
                cmd = new SqlCommand("INSERT INTO aluno (situacao, idPessoa, matricula, idCurso) VALUES (@v1, @v2, @v3, @v4);", con);
                cmd.Parameters.AddWithValue("@v1", a.situacao);
                cmd.Parameters.AddWithValue("@v2", a.idPessoa);
                cmd.Parameters.AddWithValue("@v3", a.matricula);
                cmd.Parameters.AddWithValue("@v4", a.curso.idCurso);
                cmd.ExecuteNonQuery();

                //MYSQL

                //Grava o Login MySql
                /* myCmd = new MySqlCommand("INSERT INTO login (usuario, senha, perfilAcesso) VALUES (@v1, @v2, @v3);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.login.usuario);
                myCmd.Parameters.AddWithValue("@v2", a.login.senha);
                myCmd.Parameters.AddWithValue("@v3", a.login.perfilAcesso);
                myCmd.ExecuteNonQuery();
                int idLogin = Convert.ToInt32(myCmd.LastInsertedId);

                //Gravar o Documento no MySql
                myCmd = new MySqlCommand("INSERT INTO documento (cpf, rg, dataExpedicao, orgaoExpedidor, numCertidao, livroCertidao, folhaCertidao, dataEmiCertidao, titEleitor, certReservista) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.documento.cpf);
                myCmd.Parameters.AddWithValue("@v2", a.documento.rg);
                myCmd.Parameters.AddWithValue("@v3", a.documento.dataExpedicao);
                myCmd.Parameters.AddWithValue("@v4", a.documento.orgaoExpedidor);
                myCmd.Parameters.AddWithValue("@v5", a.documento.numCertidao);
                myCmd.Parameters.AddWithValue("@v6", a.documento.livroCertidao);
                myCmd.Parameters.AddWithValue("@v7", a.documento.folhaCertidao);
                myCmd.Parameters.AddWithValue("@v8", a.documento.dataEmiCertidao);
                myCmd.Parameters.AddWithValue("@v9", a.documento.titEleitor);
                myCmd.Parameters.AddWithValue("@v10", a.documento.certReservista);
                myCmd.ExecuteNonQuery();
                int idDocumento = Convert.ToInt32(myCmd.LastInsertedId);

                //Grava o Endereco no MySql
                myCmd = new MySqlCommand("INSERT INTO endereco (logradouro, numero, complemento, bairro, cidade, uf, cep, municipio, zona) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.endereco.longradouro);
                myCmd.Parameters.AddWithValue("@v2", a.endereco.numero);
                myCmd.Parameters.AddWithValue("@v3", a.endereco.complemento);
                myCmd.Parameters.AddWithValue("@v4", a.endereco.bairro);
                myCmd.Parameters.AddWithValue("@v5", a.endereco.cidade);
                myCmd.Parameters.AddWithValue("@v6", a.endereco.uf);
                myCmd.Parameters.AddWithValue("@v7", a.endereco.cep);
                myCmd.Parameters.AddWithValue("@v8", a.endereco.municipio);
                myCmd.Parameters.AddWithValue("@v9", a.endereco.zona);
                myCmd.ExecuteNonQuery();
                int idEndereco = Convert.ToInt32(myCmd.LastInsertedId);

                //Grava o Contato no MySql
                myCmd = new MySqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular, email, outros) VALUES (@v1, @v2, @v3, @v4);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.contato.telefoneFixo);
                myCmd.Parameters.AddWithValue("@v2", a.contato.telefoneCelular);
                myCmd.Parameters.AddWithValue("@v3", a.contato.email);
                myCmd.Parameters.AddWithValue("@v4", a.contato.outros);
                myCmd.ExecuteNonQuery();
                int idContato = Convert.ToInt32(myCmd.LastInsertedId);

                //Pessoa no MySql
                myCmd = new MySqlCommand("INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento, idProgramaSocial) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.nome);
                myCmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                myCmd.Parameters.AddWithValue("@v3", a.sexo);
                myCmd.Parameters.AddWithValue("@v4", a.naturalidade);
                myCmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                myCmd.Parameters.AddWithValue("@v6", a.nomePai);
                myCmd.Parameters.AddWithValue("@v7", a.nomeMae);
                myCmd.Parameters.AddWithValue("@v8", a.etnia);
                myCmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                myCmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                myCmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                myCmd.Parameters.AddWithValue("@v12", idLogin);
                myCmd.Parameters.AddWithValue("@v13", idContato);
                myCmd.Parameters.AddWithValue("@v14", idEndereco);
                myCmd.Parameters.AddWithValue("@v15", idDocumento);
                myCmd.Parameters.AddWithValue("@v16", a.programaSocial.idProgramaSocial);
                myCmd.ExecuteNonQuery();
                int idPessoa = Convert.ToInt32(myCmd.LastInsertedId);
                
                //Aluno MySql
                myCmd = new MySqlCommand("INSERT INTO aluno (situacao, idPessoa, matricula, idCurso) VALUES (@v1, @v2, @v3, @v4);", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.situacao);
                myCmd.Parameters.AddWithValue("@v2", idPessoa);
                myCmd.Parameters.AddWithValue("@v3", a.matricula);
                myCmd.Parameters.AddWithValue("@v4", a.curso.idCurso);
                myCmd.ExecuteNonQuery(); */

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public void Alterar(Aluno a)
        {

            try
            {
                AbrirConexao();

                //SQL SERVER

                //Gravar o Login Sql Server
                cmd = new SqlCommand("UPDATE login SET usuario = @v1, senha = @v2 where idLogin = @v3", con);
                cmd.Parameters.AddWithValue("@v1", a.login.usuario);
                cmd.Parameters.AddWithValue("@v2", a.login.senha);
                cmd.Parameters.AddWithValue("@v3", a.login.idLogin);
                cmd.ExecuteNonQuery();

                //Gravar o Documento no Sql Server
                cmd = new SqlCommand("UPDATE documento SET cpf = @v1, rg = @v2, dataExpedicao = @v3, orgaoExpedidor = @v4, numCertidao = @v5, livroCertidao = @v6, folhaCertidao = @v7, dataEmiCertidao = @v8, titEleitor = @v9, certReservista = @v10 where idDocumento = @v11", con);
                cmd.Parameters.AddWithValue("@v1", a.documento.cpf);
                cmd.Parameters.AddWithValue("@v2", a.documento.rg);
                cmd.Parameters.AddWithValue("@v3", a.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", a.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", a.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", a.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", a.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", a.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", a.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", a.documento.certReservista);
                cmd.Parameters.AddWithValue("@v11", a.documento.idDocumento);
                cmd.ExecuteNonQuery();

                //Gravar o Endereco no Sql server
                cmd = new SqlCommand("UPDATE endereco SET logradouro = @v1, numero = @v2, complemento = @v3, bairro = @v4, cidade = @v5, uf = @v6, cep = @v7, municipio = @v8, zona = @v9 where idEndereco = @v10", con);
                cmd.Parameters.AddWithValue("@v1", a.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", a.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", a.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", a.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", a.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", a.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", a.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", a.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", a.endereco.zona);
                cmd.Parameters.AddWithValue("@v10", a.endereco.idEndereco);
                cmd.ExecuteNonQuery();

                //Gravar o Contato no Sql Server
                cmd = new SqlCommand("UPDATE contato SET telefoneFixo = @v1, telefoneCelular = @v2, email = @v3, outros = @v4 where idContato = @v5", con);
                cmd.Parameters.AddWithValue("@v1", a.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", a.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", a.contato.email);
                cmd.Parameters.AddWithValue("@v4", a.contato.outros);
                cmd.Parameters.AddWithValue("@v5", a.contato.idContato);
                cmd.ExecuteNonQuery();

                //Pessoa no Sql Server
                cmd = new SqlCommand("UPDATE pessoa SET nomeP = @v1, dataNascimento = @v2, sexo = @v3, naturalidade = @v4, nacionalidade = @v5, nomePai = @v6, nomeMae = @v7, etnia = @v8, estadoCivil = @v9, nivelEscolaridade = @v10, necessidadeEsp = @v11 where idPessoa = @v12", con);
                cmd.Parameters.AddWithValue("@v1", a.nome);
                cmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", a.sexo);
                cmd.Parameters.AddWithValue("@v4", a.naturalidade);
                cmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", a.nomePai);
                cmd.Parameters.AddWithValue("@v7", a.nomeMae);
                cmd.Parameters.AddWithValue("@v8", a.etnia);
                cmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", a.idPessoa);
                cmd.ExecuteNonQuery();

                //Aluno Sql Server
                cmd = new SqlCommand("UPDATE aluno SET situacao = @v1, idCurso = @v2 where idAluno = @v3;", con);
                cmd.Parameters.AddWithValue("@v1", a.situacao);
                cmd.Parameters.AddWithValue("@v2", a.curso.idCurso);
                cmd.Parameters.AddWithValue("@v3", a.idAluno);
                cmd.ExecuteNonQuery();

                //MYSQL

                //Grava o Login MySql
                /* myCmd = new MySqlCommand("UPDATE login SET usuario = @v1, senha = @v2 where idLogin = @v3;", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.login.usuario);
                myCmd.Parameters.AddWithValue("@v2", a.login.senha);
                myCmd.Parameters.AddWithValue("@v3", a.login.idLogin);
                myCmd.ExecuteNonQuery();

                //Gravar o Documento no MySql
                myCmd = new MySqlCommand("UPDATE documento SET cpf = @v1, rg = @v2, dataExpedicao = @v3, orgaoExpedidor = @v4, numCertidao = @v5, livroCertidao = @v6, folhaCertidao = @v7, dataEmiCertidao = @v8, titEleitor = @v9, certReservista = @v10 where idDocumento = @v11", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.documento.cpf);
                myCmd.Parameters.AddWithValue("@v2", a.documento.rg);
                myCmd.Parameters.AddWithValue("@v3", a.documento.dataExpedicao);
                myCmd.Parameters.AddWithValue("@v4", a.documento.orgaoExpedidor);
                myCmd.Parameters.AddWithValue("@v5", a.documento.numCertidao);
                myCmd.Parameters.AddWithValue("@v6", a.documento.livroCertidao);
                myCmd.Parameters.AddWithValue("@v7", a.documento.folhaCertidao);
                myCmd.Parameters.AddWithValue("@v8", a.documento.dataEmiCertidao);
                myCmd.Parameters.AddWithValue("@v9", a.documento.titEleitor);
                myCmd.Parameters.AddWithValue("@v10", a.documento.certReservista);
                myCmd.Parameters.AddWithValue("@v11", a.documento.idDocumento);
                myCmd.ExecuteNonQuery();

                //Grava o Endereco no MySql
                myCmd = new MySqlCommand("UPDATE endereco SET logradouro = @v1, numero = @v2, complemento = @v3, bairro = @v4, cidade = @v5, uf = @v6, cep = @v7, municipio = @v8, zona = @v9 where idEndereco = @v10", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.endereco.longradouro);
                myCmd.Parameters.AddWithValue("@v2", a.endereco.numero);
                myCmd.Parameters.AddWithValue("@v3", a.endereco.complemento);
                myCmd.Parameters.AddWithValue("@v4", a.endereco.bairro);
                myCmd.Parameters.AddWithValue("@v5", a.endereco.cidade);
                myCmd.Parameters.AddWithValue("@v6", a.endereco.uf);
                myCmd.Parameters.AddWithValue("@v7", a.endereco.cep);
                myCmd.Parameters.AddWithValue("@v8", a.endereco.municipio);
                myCmd.Parameters.AddWithValue("@v9", a.endereco.zona);
                myCmd.Parameters.AddWithValue("@v10", a.endereco.idEndereco);
                myCmd.ExecuteNonQuery();

                //Grava o Contato no MySql
                myCmd = new MySqlCommand("UPDATE contato SET telefoneFixo = @v1, telefoneCelular = @v2, email = @v3, outros = @v4 where idContato = @v5", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.contato.telefoneFixo);
                myCmd.Parameters.AddWithValue("@v2", a.contato.telefoneCelular);
                myCmd.Parameters.AddWithValue("@v3", a.contato.email);
                myCmd.Parameters.AddWithValue("@v4", a.contato.outros);
                myCmd.Parameters.AddWithValue("@v5", a.contato.idContato);
                myCmd.ExecuteNonQuery();

                //Pessoa no MySql
                myCmd = new MySqlCommand("UPDATE pessoa SET nomeP = @v1, dataNascimento = @v2, sexo = @v3, naturalidade = @v4, nacionalidade = @v5, nomePai = @v6, nomeMae = @v7, etnia = @v8, estadoCivil = @v9, nivelEscolaridade = @v10, necessidadeEsp = @v11 where idPessoa = @v12", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.nome);
                myCmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                myCmd.Parameters.AddWithValue("@v3", a.sexo);
                myCmd.Parameters.AddWithValue("@v4", a.naturalidade);
                myCmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                myCmd.Parameters.AddWithValue("@v6", a.nomePai);
                myCmd.Parameters.AddWithValue("@v7", a.nomeMae);
                myCmd.Parameters.AddWithValue("@v8", a.etnia);
                myCmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                myCmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                myCmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                myCmd.Parameters.AddWithValue("@v12", a.idPessoa);
                myCmd.ExecuteNonQuery();

                //Aluno MySql
                myCmd = new MySqlCommand("UPDATE aluno SET situacao = @v1, idCurso = @v2 where idAluno = @v3;", myCon);
                myCmd.Parameters.AddWithValue("@v1", a.situacao);
                myCmd.Parameters.AddWithValue("@v2", a.curso.idCurso);
                myCmd.Parameters.AddWithValue("@v3", a.idAluno);
                myCmd.ExecuteNonQuery(); */

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public Aluno buscarAlunoMatricula(string matricula)
        {
            try
            {
                AbrirConexao();

                Aluno a = new Aluno();

                cmd = new SqlCommand(
                        "SELECT * " +
                        "FROM aluno a INNER JOIN pessoa p " +
                        "ON a.idPessoa = p.idPessoa " +
                        "INNER JOIN login l " +
                        "ON l.idLogin = p.idLogin " +
                        "INNER JOIN documento d " +
                        "ON d.idDocumento = p.idDocumento " +
                        "INNER JOIN contato c " +
                        "ON c.idContato = p.idContato " +
                        "INNER JOIN endereco e " +
                        "ON e.idEndereco = p.idEndereco " +
                        "INNER JOIN curso cu " +
                        "ON cu.idCurso = a.idCurso " +
                        "INNER JOIN programaSocial ps " +
                        "ON ps.idProgramaSocial = p.idProgramaSocial " +
                        "WHERE a.matricula = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", matricula);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.nome = Convert.ToString(dr["nomeP"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                }

                return a;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public Aluno buscarAlunoCpf(string cpf)
        {
            try
            {
                AbrirConexao();

                Aluno a = new Aluno();

                cmd = new SqlCommand(
                        "SELECT * " +
                        "FROM aluno a INNER JOIN pessoa p " +
                        "ON a.idPessoa = p.idPessoa " +
                        "INNER JOIN login l " +
                        "ON l.idLogin = p.idLogin " +
                        "INNER JOIN documento d " +
                        "ON d.idDocumento = p.idDocumento " +
                        "INNER JOIN contato c " +
                        "ON c.idContato = p.idContato " +
                        "INNER JOIN endereco e " +
                        "ON e.idEndereco = p.idEndereco " +
                        "INNER JOIN curso cu " +
                        "ON cu.idCurso = a.idCurso " +
                        "INNER JOIN programaSocial ps " +
                        "ON ps.idProgramaSocial = p.idProgramaSocial " +
                        "WHERE d.cpf = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", cpf);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.nome = Convert.ToString(dr["nomeP"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                }

                return a;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public Aluno buscarAlunoRg(string rg)
        {
            try
            {
                AbrirConexao();

                Aluno a = new Aluno();

                cmd = new SqlCommand(
                        "SELECT * " +
                        "FROM aluno a INNER JOIN pessoa p " +
                        "ON a.idPessoa = p.idPessoa " +
                        "INNER JOIN login l " +
                        "ON l.idLogin = p.idLogin " +
                        "INNER JOIN documento d " +
                        "ON d.idDocumento = p.idDocumento " +
                        "INNER JOIN contato c " +
                        "ON c.idContato = p.idContato " +
                        "INNER JOIN endereco e " +
                        "ON e.idEndereco = p.idEndereco " +
                        "INNER JOIN curso cu " +
                        "ON cu.idCurso = a.idCurso " +
                        "INNER JOIN programaSocial ps " +
                        "ON ps.idProgramaSocial = p.idProgramaSocial " +
                        "WHERE d.rg = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", rg);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.nome = Convert.ToString(dr["nomeP"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                }

                return a;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<Aluno> ListarAluno()
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand(
                                "SELECT * " +
                                    "FROM aluno a INNER JOIN pessoa p " +
                                    "ON a.idPessoa = p.idPessoa " +
                                    "INNER JOIN login l " +
                                    "ON l.idLogin = p.idLogin " +
                                    "INNER JOIN documento d " +
                                    "ON d.idDocumento = p.idDocumento " +
                                    "INNER JOIN contato c " +
                                    "ON c.idContato = p.idContato " +
                                    "INNER JOIN endereco e " +
                                    "ON e.idEndereco = p.idEndereco " +
                                    "INNER JOIN curso cu " +
                                    "ON cu.idCurso = a.idCurso " +
                                    "INNER JOIN programaSocial ps " +
                                    "ON ps.idProgramaSocial = p.idProgramaSocial ", con);
                dr = cmd.ExecuteReader();

                List<Aluno> listaAluno = new List<Aluno>();

                while (dr.Read())
                {

                    Aluno a = new Aluno();
                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.login.perfilAcesso = Convert.ToString(dr["perfilAcesso"]);
                    a.nome = Convert.ToString(dr["nomeP"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.nomeMae = Convert.ToString(dr["nomeMae"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                    a.programaSocial.idProgramaSocial = Convert.ToInt32(dr["idProgramaSocial"]);
                    a.curso.idCurso = Convert.ToInt32(dr["idProgramaSocial"]);
                    listaAluno.Add(a);
                }
                return listaAluno;
            }
            catch
            {

                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}